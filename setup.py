from setuptools import setup

setup(
    # Package info
    name='DNASequencer',
    version='0.1.0',
    url='https://gitlab.com/patrickdlee/dna-sequencer',
    license='MIT',
    packages=['sequencer'],
    description='Sequence DNA fragments from file in FASTA format.',
    long_description=open('README.md').read(),

    # Author info
    author='Patrick Lee',
    author_email='alimadzi@gmail.com',

    # Requirements
    install_requires=[],
    extras_require={
        'test': [
            'pylint>=1.7.4',
            'pytest>=3.2.2',
            'pytest-cov>=2.5.1',
            'tox>=2.9.1',
        ],
    },
)
