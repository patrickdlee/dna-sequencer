"""
FASTALoader
"""

import logging
import re

from sequencer.dna_fragment import DNAFragment
from sequencer.exceptions import InvalidFASTAFormatError


class FASTALoader(object):  # pylint: disable=too-few-public-methods
    """Load DNA sequences from FASTA file"""

    def __init__(self, file_path):
        """
        Initialize a new FASTALoader

        :param file_path Path to the data file
        """
        self.file_path = file_path
        self.logger = logging.getLogger(__name__)

    def load_sequences(self):
        """
        Load DNA sequences from a text file in FASTA format

        :return: List of DNA sequences
        """
        self.logger.info("Opening file: " + self.file_path)
        sequence_file = open(self.file_path, 'r')
        lines = sequence_file.readlines()

        sequences = []
        content = ''

        self.logger.info("Loading sequences from file: " + self.file_path)
        for line in lines:
            line = line.strip('\n')

            if line[0] == '>':
                if content:
                    sequences.append(DNAFragment(content))
                content = ''
                continue

            if not re.match(r"^[ACGT]+$", line):
                raise InvalidFASTAFormatError(line)

            content += line

        sequences.append(DNAFragment(content))
        self.logger.info("Loaded " + str(len(sequences)) + " sequences")

        return sequences
