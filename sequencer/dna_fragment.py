"""
DNAFragment
"""

import math


class DNAFragment(object):  # pylint: disable=too-few-public-methods
    """A fragment of DNA"""

    def __init__(self, fragment):
        """
        Initialize a new DNA fragment

        :param fragment: The content of the fragment
        """
        self.content = fragment
        self.left_overlaps = set()
        self.right_overlaps = set()

        # Fragments must overlap by more than half of each fragment's length
        min_overlap_length = int(math.floor(len(fragment) / 2) + 1)

        # Populate the left and right overlap sets
        for i in range(min_overlap_length, len(fragment)):
            self.left_overlaps.add(fragment[:i])
            self.right_overlaps.add(fragment[-i:])
