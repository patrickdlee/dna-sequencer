"""
DNA Sequencer
-------------
This module contains classes and methods to load DNA fragment data from a file
in FASTA format and output the chromosome built from those fragments.

Author: Patrick Lee (alimadzi@gmail.com)
URL:    https://gitlab.com/patrickdlee/dna-sequencer
"""

import logging.config
import os.path

LOGGING_CONF = os.path.join(os.path.dirname(__file__), 'logging.ini')
logging.config.fileConfig(LOGGING_CONF)
