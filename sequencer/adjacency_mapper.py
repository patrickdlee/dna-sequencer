"""
AdjacencyMapper
"""

import logging


class AdjacencyMapper(object):  # pylint: disable=too-few-public-methods
    """Maps possible neighbors among DNA fragments"""

    def __init__(self, fragments):
        """
        Initialize a new AdjacencyMapper

        :param fragments List of DNA fragments
        """
        self.fragments = fragments
        self.logger = logging.getLogger(__name__)

    def build_adjacency_map(self):
        """
        Build an adjacency map for a list of DNA fragments

        :return Adjacency map
        """
        self.logger.info("Building adjacency map for " + str(len(self.fragments)) + " fragments")

        adjacency_map = {}
        for fragment in self.fragments:
            neighbors = {}
            for possible_neighbor in self.fragments:
                if fragment == possible_neighbor:
                    continue

                overlap_intersection = fragment.right_overlaps\
                                               .intersection(possible_neighbor.left_overlaps)

                if overlap_intersection:
                    overlap = overlap_intersection.pop()
                    neighbors[possible_neighbor] = overlap

            adjacency_map[fragment] = neighbors

        self.logger.info("Built adjacency map with " + str(len(adjacency_map)) + " elements")
        return adjacency_map
