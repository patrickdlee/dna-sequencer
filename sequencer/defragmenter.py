"""
Defragmenter
"""

import logging
from multiprocessing.dummy import Pool
from multiprocessing.pool import ThreadPool

from sequencer.exceptions import UnableToSequenceError


class Defragmenter(object):
    """Defragment a list of DNA fragments"""

    def __init__(self, adjacency_map):
        """
        Initialize a new Defragmenter

        :param adjacency_map Map of adjacent DNA fragments
        """
        self.adjacency_map = adjacency_map
        self.logger = logging.getLogger(__name__)

    def order_fragments(self):
        """
        Analyze an adjacency map and determine correct order of DNA fragments

        :return: Ordered list of fragments in a chromosome
        """
        self.logger.info("Ordering " + str(len(self.adjacency_map)) + " fragments")

        for fragment in self.adjacency_map.keys():
            fragments = self.explore_fragment_order(fragment)
            if fragments:
                self.logger.info("Correct starting fragment: " + fragment.content)
                return fragments

        raise UnableToSequenceError("Fragments could not be ordered")

    def order_fragments_multi_threaded(self):
        """
        Analyze an adjacency map and determine correct order of DNA fragments
        NOTE: This method uses multiple threads

        :return: Ordered list of fragments in a chromosome
        """
        self.logger.info("Ordering " + str(len(self.adjacency_map)) + " fragments (multi-threaded)")

        pool = ThreadPool()
        results = pool.map(self.explore_fragment_order, self.adjacency_map.keys())
        pool.close()
        pool.join()
        correct_order = next((item for item in results if item is not None), None)
        if correct_order:
            self.logger.info("Correct starting fragment: " + correct_order[0].content)
            return correct_order

        raise UnableToSequenceError("Fragments could not be ordered")

    def order_fragments_multi_process(self):
        """
        Analyze an adjacency map and determine correct order of DNA fragments
        NOTE: This method uses multiple processes

        :return: Ordered list of fragments in a chromosome
        """
        self.logger.info("Ordering " + str(len(self.adjacency_map)) + " fragments (multi-process)")

        pool = Pool(len(self.adjacency_map))
        results = pool.map(self.explore_fragment_order, self.adjacency_map.keys())
        pool.close()
        pool.join()
        correct_order = next((item for item in results if item is not None), None)
        if correct_order:
            self.logger.info("Correct starting fragment: " + correct_order[0].content)
            return correct_order

        raise UnableToSequenceError("Fragments could not be ordered")

    def explore_fragment_order(self, fragment):
        """
        Explore possible ordering of fragments in an adjacency map starting with
        the given fragment

        :param fragment DNA fragment
        :return: Ordered list of fragments in a chromosome
        """
        self.logger.info("Trying to order fragments starting with " + fragment.content)
        fragment_orderings = [[fragment]]

        while fragment_orderings:
            fragments = fragment_orderings.pop()

            for neighbor in self.adjacency_map[fragments[-1]].keys():
                if neighbor in fragments:
                    continue

                new_fragments = fragments + [neighbor]
                if len(new_fragments) == len(self.adjacency_map):
                    return new_fragments
                fragment_orderings.append(new_fragments)

        self.logger.info("Fragments could not be ordered starting with " + fragment.content)
        return None
