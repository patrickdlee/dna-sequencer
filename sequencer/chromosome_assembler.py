"""
ChromosomeAssembler
"""

import logging


class ChromosomeAssembler(object):  # pylint: disable=too-few-public-methods
    """Assemble a chromosome"""

    def __init__(self, adjacency_map, fragments):
        """
        Initialize a new ChromosomeAssembler

        :param adjacency_map Map of adjacent DNA fragments
        :param fragments     Ordered list of DNA fragments
        """
        self.adjacency_map = adjacency_map
        self.fragments = fragments
        self.logger = logging.getLogger(__name__)

    def assemble_chromosome(self):
        """
        Assemble a chromosome from an ordered list of fragments

        :return: Chromosome as a string
        """
        self.logger.info("Assembling chromosome from " + str(len(self.fragments)) + " fragments")
        prev_fragment = self.fragments.pop(0)
        chromosome = prev_fragment.content

        for fragment in self.fragments:
            overlap_size = len(self.adjacency_map[prev_fragment][fragment])
            chromosome += fragment.content[overlap_size:]
            prev_fragment = fragment

        self.logger.info("Assembled chromosome")
        return chromosome
