"""
Custom exceptions for DNA Sequencer module
"""

import logging


class DNASequencerError(Exception):
    """
    Base exception for DNA Sequencer
    """
    def __init__(self, message):
        super(DNASequencerError, self).__init__(message)
        logging.getLogger(__name__).error(message)


class InvalidFASTAFormatError(DNASequencerError):
    """
    FASTA file contains text other than comments and A, C, G, and T
    """
    def __init__(self, invalid_text):
        super(InvalidFASTAFormatError, self).__init__("Invalid FASTA format: %s" % invalid_text)


class UnableToSequenceError(DNASequencerError):
    """
    Unable to find a valid sequence for a set of DNA fragments
    """
