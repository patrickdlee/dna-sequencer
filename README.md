# DNA Sequencer Coding Challenge

## Requirements

This module has been tested with Python 2.7 and 3.6 and should be compatible with other Python 3 versions. It requires Pip and has the following dependencies as listed in the `requirements.txt` file:
```
pylint>=1.7.4
pytest>=3.2.2
pytest-cov>=2.5.1
tox>=2.9.1
```

## Setup

There is a `Makefile` in the root directory to facilitate common tasks. To initialize an environment to run the DNA Sequencer, use the following command...
```
make init
```
This will run `pip install -r requirements.txt` and install the Pip packages needed by the module.

## Usage

### Run the Sequencer

Once the required Pip packages are installed, the sequencer will be ready to run. Execute the following command to generate the chromosome from the challenge data set...
```
./sequencer.py files/coding_challenge_data_set.txt
```

### Test the Sequencer

The `Makefile` is set up to run various other commands...

- `make test`: run all the PyTest tests
- `make cover`: generate a code coverage report (should be 100%)
- `make lint`: run PyLint and output the results
- `make dist`: build a source distribution of the module

However, the simplest way to put the module through its paces is to run Tox. Copy and paste the following complicated command into the terminal and execute it...
```
tox
```
This will run all of the unit tests using both Python 2.7 and 3.6, generate a code coverage report, and finally run PyLint. If a different release of Python 3 is installed on your machine, just edit the environment list at the top of the `tox.ini` file...
```
[tox]
envlist = py27,py36,cov,lint
```
For example, if you have Python 3.3.7 installed, just replace `py36` with `py33`. All of the unit tests should pass with any recent release of Python 3.

## How It Works

My basic approach to this challenge was to use Test Driven Development (TDD) to validate that my code was processing the input correctly. In order to facilitate TDD, I broke up the challenge into four steps so they could be tested in isolation.

1. Load DNA sequences from a file in FASTA format and enumerate all of the possible left and right "overlaps" that each fragment may have with a neighbor.
2. Build an "adjacency map" that lists all of the possible right neighbors for each fragment with their overlaps.
3. Determine the correct fragment order by exploring all possible orderings starting with each fragment.
4. Assemble the chromosome from the correct fragment order and overlaps from the adjacency map.

Each of these steps has specific inputs and outputs that can be validated independently from the other steps.

### FASTA Loader

The `FASTALoader` class takes the path to a file as input. It then opens the file and reads the sequences line by line. Each sequence is stored in a `DNAFragment` object. Part of the initialization of a `DNAFragment` is to enumerate the possible left and right overlaps and store them in the object. The output of the `FASTALoader` is an unordered list of `DNAFragment` objects.

Adjacent fragments must overlap by more than half their length. So the fragment `ACTGC` has left overlaps of `ACT` and `ACTG` as well as right overlaps of `TGC` and `CTGC`.

### Adjacency Mapper

The `AdjacencyMapper` class performs a set intersection operation on every ordered pair of fragments to determine which pairs can overlap. For example, the fragment `ACTGC` from above can overlap with the fragment `TGCAA` because `TGC` is in the set of right overlaps for `ACTGC` and also in the set of left overlaps for `TGCAA`.

The output of the `AdjacencyMapper` is a dictionary of the possible overlapping fragments where the key is a DNA fragment and the value is a dictionary of the fragments that could follow that fragment as well as the overlap for each pair.

### Defragmenter

The `Defragmenter` class uses the adjacency map to attempt to build a chromosome using all of the DNA fragments. For every fragment, it uses a stack to systematically explore all possible orderings starting with that fragment. If a full chromosome cannot start with a particular fragment, the defragmenter moves on to the next one until it is successful.

This exploration starting with each fragment can be performed sequentially in a single thread (`order_fragments()`) or performed in parallel using either multiple threads (`order_fragments_multi_threaded()`) or multiple sub-processes (`order_fragments_multi_process()`). The unit tests in `test_defragmenter.py` exercise all of these options multiple times. The output of the `Defragmenter` is an ordered list of DNA fragments ready to be assembled into a chromosome.

Given the relatively small input data set in the challenge, the overhead of creating multiple threads or sub-processes outweighs any performance gain from parallelizing the search algorithm. However, a much larger data set would probably benefit from parallel processing, so those options are available in the module.

### Chromosome Assembler

The `ChromosomeAssembler` takes the ordered list of DNA fragments from the `Defragmenter`, starts with the first fragment in the list, and then appends each one after removing the overlap from the left side. The resulting sequence is the full chromosome.
