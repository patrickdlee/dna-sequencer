#!/usr/bin/env python
"""
DNA Sequencer
-------------
This script takes a file in FASTA format as input and outputs the chromosome
built from the DNA fragments in the file.

Usage:
    ./sequencer.py path/to/data_file
"""

import argparse

from sequencer.adjacency_mapper import AdjacencyMapper
from sequencer.chromosome_assembler import ChromosomeAssembler
from sequencer.defragmenter import Defragmenter
from sequencer.exceptions import InvalidFASTAFormatError
from sequencer.exceptions import UnableToSequenceError
from sequencer.fasta_loader import FASTALoader


parser = argparse.ArgumentParser(description='Sequence chromosomes from sets of DNA fragments')
parser.add_argument('fasta_file', metavar='PATH_TO_FILE', type=str, nargs=1,
                    help='path to a data file in FASTA format')
args = parser.parse_args()

try:
    # Load the data set from a file in FASTA format
    loader = FASTALoader(args.fasta_file[0])
    fragments = loader.load_sequences()

    # Build an adjacency map from the fragments
    mapper = AdjacencyMapper(fragments)
    adjacency_map = mapper.build_adjacency_map()

    # Order the fragments in the adjacency map
    defragmenter = Defragmenter(adjacency_map)
    ordered_fragments = defragmenter.order_fragments()

    # Build the resulting chromosome
    assembler = ChromosomeAssembler(adjacency_map, ordered_fragments)
    chromosome = assembler.assemble_chromosome()

    # Output the chromosome
    print(chromosome)
except InvalidFASTAFormatError:
    print('The file \'%s\' is not valid FASTA format.' % args.fasta_file[0])
    exit(1)
except UnableToSequenceError:
    print('Unable to sequence the fragments provided.')
    exit(2)

exit(0)
