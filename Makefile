init:
	pip install -r requirements.txt

test:
	pytest -vv

cover:
	pytest --cov sequencer

lint:
	pylint --reports=y sequencer

dist:
	python setup.py sdist
