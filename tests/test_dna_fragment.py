from sequencer.dna_fragment import DNAFragment


class TestDNAFragment(object):
    """Tests for the DNAFragment class"""

    def test_overlaps_zero_length(self):
        """
        Fragment has zero length. No overlaps.
        """
        frag = DNAFragment('')
        assert frag.left_overlaps == set()
        assert frag.right_overlaps == set()

    def test_overlaps_length_one(self):
        """
        Fragment has a single character. No overlaps.
        """
        frag = DNAFragment('A')
        assert frag.left_overlaps == set()
        assert frag.right_overlaps == set()

    def test_overlaps_length_two(self):
        """
        Fragment has a length of two. No overlaps.
        """
        frag = DNAFragment('AG')
        assert frag.left_overlaps == set()
        assert frag.right_overlaps == set()

    def test_overlaps_odd_length(self):
        """
        Fragment has odd length.
        """
        frag = DNAFragment('ACGAT')
        assert frag.left_overlaps == {'ACG', 'ACGA'}
        assert frag.right_overlaps == {'GAT', 'CGAT'}

    def test_overlaps_even_length(self):
        """
        Fragment has even length.
        """
        frag = DNAFragment('ACGATC')
        assert frag.left_overlaps == {'ACGA', 'ACGAT'}
        assert frag.right_overlaps == {'GATC', 'CGATC'}
