import pytest as pytest

from sequencer.exceptions import UnableToSequenceError
from sequencer.defragmenter import Defragmenter
from sequencer.dna_fragment import DNAFragment


class TestDefragmenter(object):
    """Tests for the Defragmenter class"""

    def test_order_fragments_no_branching(self):
        """
        Each fragment has at most one possible right neighbor.
        """
        fragment1 = DNAFragment('ATTAGACCTG')
        fragment2 = DNAFragment('CCTGCCGGAA')
        fragment3 = DNAFragment('AGACCTGCCG')
        fragment4 = DNAFragment('GCCGGAATAC')

        adjacency_map = {
            fragment1: {fragment3: 'AGACCTG'},
            fragment2: {fragment4: 'GCCGGAA'},
            fragment3: {fragment2: 'CCTGCCG'},
            fragment4: {}
        }
        defragmenter = Defragmenter(adjacency_map)
        ordered_fragments = defragmenter.order_fragments()
        ordered_fragments_threaded = defragmenter.order_fragments_multi_threaded()
        ordered_fragments_multi_proc = defragmenter.order_fragments_multi_process()

        assert ordered_fragments == [fragment1, fragment3, fragment2, fragment4]
        assert ordered_fragments_threaded == [fragment1, fragment3, fragment2, fragment4]
        assert ordered_fragments_multi_proc == [fragment1, fragment3, fragment2, fragment4]

    def test_order_fragments_branching(self):
        """
         One fragment has two possible right neighbors.
        """
        fragment1 = DNAFragment('ACGTC')
        fragment2 = DNAFragment('GTCAC')
        fragment3 = DNAFragment('CGTCAA')
        fragment4 = DNAFragment('TCACGTC')

        adjacency_map = {
            fragment1: {fragment2: 'GTC', fragment3: 'CGTC'},
            fragment2: {fragment4: 'TCAC'},
            fragment3: {},
            fragment4: {fragment3: 'CGTC'}
        }
        defragmenter = Defragmenter(adjacency_map)
        ordered_fragments = defragmenter.order_fragments()
        ordered_fragments_threaded = defragmenter.order_fragments_multi_threaded()
        ordered_fragments_multi_proc = defragmenter.order_fragments_multi_process()

        assert ordered_fragments == [fragment1, fragment2, fragment4, fragment3]
        assert ordered_fragments_threaded == [fragment1, fragment2, fragment4, fragment3]
        assert ordered_fragments_multi_proc == [fragment1, fragment2, fragment4, fragment3]

    def test_order_fragments_circular(self):
        """
        Two fragments can each follow the other in a circular arrangement.
        """
        fragment1 = DNAFragment('GACGACG')
        fragment2 = DNAFragment('CGACGA')
        fragment3 = DNAFragment('GACGTT')

        adjacency_map = {
            fragment1: {fragment2: 'CGACG', fragment3: 'GACG'},
            fragment2: {fragment1: 'GACGA'},
            fragment3: {}
        }
        defragmenter = Defragmenter(adjacency_map)
        ordered_fragments = defragmenter.order_fragments()
        ordered_fragments_threaded = defragmenter.order_fragments_multi_threaded()
        ordered_fragments_multi_proc = defragmenter.order_fragments_multi_process()

        assert ordered_fragments == [fragment2, fragment1, fragment3]
        assert ordered_fragments_threaded == [fragment2, fragment1, fragment3]
        assert ordered_fragments_multi_proc == [fragment2, fragment1, fragment3]

    def test_order_fragments_impossible(self):
        """
        No possible ordering for this set of fragments.
        """
        fragment1 = DNAFragment('ACGT')
        fragment2 = DNAFragment('CGTC')
        fragment3 = DNAFragment('GAGAG')

        adjacency_map = {
            fragment1: {fragment2: 'CGT'},
            fragment2: {},
            fragment3: {}
        }
        defragmenter = Defragmenter(adjacency_map)

        with pytest.raises(UnableToSequenceError):
            defragmenter.order_fragments()
        with pytest.raises(UnableToSequenceError):
            defragmenter.order_fragments_multi_threaded()
        with pytest.raises(UnableToSequenceError):
            defragmenter.order_fragments_multi_process()
