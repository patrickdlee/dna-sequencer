import pytest as pytest

from sequencer.exceptions import InvalidFASTAFormatError
from sequencer.fasta_loader import FASTALoader


class TestFASTALoader(object):
    """Tests for the FASTALoader class"""

    def test_load_single_line_sequences(self):
        """
        Each sequence in the file is on a single line.
        """
        loader = FASTALoader('files/sample1.txt')
        sequences = loader.load_sequences()

        assert sequences[0].content == 'ACGTC'
        assert sequences[1].content == 'GTCAC'
        assert sequences[2].content == 'CGTCAA'
        assert sequences[3].content == 'TCACGTC'

    def test_load_multi_line_sequences(self):
        """
        Each sequence in the file spans multiple lines.
        """
        loader = FASTALoader('files/sample2.txt')
        sequences = loader.load_sequences()

        assert sequences[0].content == 'ATTAGACCTG'
        assert sequences[1].content == 'CCTGCCGGAA'
        assert sequences[2].content == 'AGACCTGCCG'
        assert sequences[3].content == 'GCCGGAATAC'

    def test_load_invalid_sequence(self):
        """
        The file includes a sequence with invalid characters.
        """
        with pytest.raises(InvalidFASTAFormatError, match=r'AGACCTGXXG'):
            loader = FASTALoader('files/sample3.txt')
            loader.load_sequences()
