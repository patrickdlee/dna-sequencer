from sequencer.adjacency_mapper import AdjacencyMapper
from sequencer.dna_fragment import DNAFragment


class TestAdjacencyMapper(object):
    """Tests for the AdjacencyMapper class"""

    def test_build_adjacency_map_no_branching(self):
        """
        Each fragment has at most one possible right neighbor.
        """
        fragment1 = DNAFragment('ATTAGACCTG')
        fragment2 = DNAFragment('CCTGCCGGAA')
        fragment3 = DNAFragment('AGACCTGCCG')
        fragment4 = DNAFragment('GCCGGAATAC')

        mapper = AdjacencyMapper([fragment1, fragment2, fragment3, fragment4])
        adjacency_map = mapper.build_adjacency_map()

        # Correct ordering is 1, 3, 2, 4
        assert len(adjacency_map[fragment1]) == 1
        assert fragment3 in adjacency_map[fragment1].keys()
        assert adjacency_map[fragment1][fragment3] == 'AGACCTG'

        assert len(adjacency_map[fragment2]) == 1
        assert fragment4 in adjacency_map[fragment2].keys()
        assert adjacency_map[fragment2][fragment4] == 'GCCGGAA'

        assert len(adjacency_map[fragment3]) == 1
        assert fragment2 in adjacency_map[fragment3].keys()
        assert adjacency_map[fragment3][fragment2] == 'CCTGCCG'

        assert len(adjacency_map[fragment4]) == 0

    def test_build_adjacency_map_branching(self):
        """
         One fragment has two possible right neighbors.
        """
        fragment1 = DNAFragment('ACGTC')
        fragment2 = DNAFragment('GTCAC')
        fragment3 = DNAFragment('CGTCAA')
        fragment4 = DNAFragment('TCACGTC')

        mapper = AdjacencyMapper([fragment1, fragment2, fragment3, fragment4])
        adjacency_map = mapper.build_adjacency_map()

        # Correct ordering is 1, 2, 4, 3
        assert len(adjacency_map[fragment1]) == 2
        assert fragment2 in adjacency_map[fragment1]
        assert adjacency_map[fragment1][fragment2] == 'GTC'
        assert fragment3 in adjacency_map[fragment1]
        assert adjacency_map[fragment1][fragment3] == 'CGTC'

        assert len(adjacency_map[fragment2]) == 1
        assert fragment4 in adjacency_map[fragment2]
        assert adjacency_map[fragment2][fragment4] == 'TCAC'

        assert len(adjacency_map[fragment3]) == 0

        assert len(adjacency_map[fragment4]) == 1
        assert fragment3 in adjacency_map[fragment4]
        assert adjacency_map[fragment4][fragment3] == 'CGTC'

    def test_build_adjacency_map_circular(self):
        """
        Two fragments can each follow the other in a circular arrangement.
        """
        fragment1 = DNAFragment('GACGACG')
        fragment2 = DNAFragment('CGACGA')
        fragment3 = DNAFragment('GACGTT')

        mapper = AdjacencyMapper([fragment1, fragment2, fragment3])
        adjacency_map = mapper.build_adjacency_map()

        # Correct ordering is 2, 1, 3
        assert len(adjacency_map[fragment1]) == 2
        assert fragment2 in adjacency_map[fragment1]
        assert adjacency_map[fragment1][fragment2] == 'CGACG'
        assert fragment3 in adjacency_map[fragment1]
        assert adjacency_map[fragment1][fragment3] == 'GACG'

        assert len(adjacency_map[fragment2]) == 1
        assert fragment1 in adjacency_map[fragment2]
        assert adjacency_map[fragment2][fragment1] == 'GACGA'

        assert len(adjacency_map[fragment3]) == 0
