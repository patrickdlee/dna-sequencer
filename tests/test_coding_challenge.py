from sequencer.adjacency_mapper import AdjacencyMapper
from sequencer.chromosome_assembler import ChromosomeAssembler
from sequencer.defragmenter import Defragmenter
from sequencer.fasta_loader import FASTALoader


def test_coding_challenge():
    """
    Loads the data set for the coding challenge, initializes the DNA fragments,
    builds an adjacency map, orders the fragments, builds the chromosome, and
    verifies the answer.
    """
    # Load the data set for the coding challenge
    loader = FASTALoader('files/coding_challenge_data_set.txt')
    fragments = loader.load_sequences()

    # Build an adjacency map from the fragments
    mapper = AdjacencyMapper(fragments)
    adjacency_map = mapper.build_adjacency_map()

    # Order the fragments in the adjacency map
    defragmenter = Defragmenter(adjacency_map)
    ordered_fragments = defragmenter.order_fragments()

    # Build the resulting chromosome
    assembler = ChromosomeAssembler(adjacency_map, ordered_fragments)
    chromosome = assembler.assemble_chromosome()

    # Load the correct answer from a text file
    correct_answer = open('files/coding_challenge_answer.txt', 'r').read().strip('\n')

    # Verify the answer
    assert chromosome == correct_answer
