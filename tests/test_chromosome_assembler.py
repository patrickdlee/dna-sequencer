from sequencer.chromosome_assembler import ChromosomeAssembler
from sequencer.dna_fragment import DNAFragment


class TestChromosomeAssembler(object):
    """Tests for the ChromosomeAssembler class"""

    def test_build_chromosome_no_branching(self):
        """
        Each fragment has at most one possible right neighbor.
        """
        fragment1 = DNAFragment('ATTAGACCTG')
        fragment2 = DNAFragment('CCTGCCGGAA')
        fragment3 = DNAFragment('AGACCTGCCG')
        fragment4 = DNAFragment('GCCGGAATAC')

        adjacency_map = {
            fragment1: {fragment3: 'AGACCTG'},
            fragment2: {fragment4: 'GCCGGAA'},
            fragment3: {fragment2: 'CCTGCCG'},
            fragment4: {}
        }
        ordered_fragments = [fragment1, fragment3, fragment2, fragment4]
        assembler = ChromosomeAssembler(adjacency_map, ordered_fragments)
        chromosome = assembler.assemble_chromosome()

        assert chromosome == 'ATTAGACCTGCCGGAATAC'

    def test_build_chromosome_branching(self):
        """
         One fragment has two possible right neighbors.
        """
        fragment1 = DNAFragment('ACGTC')
        fragment2 = DNAFragment('GTCAC')
        fragment3 = DNAFragment('CGTCAA')
        fragment4 = DNAFragment('TCACGTC')

        adjacency_map = {
            fragment1: {fragment2: 'GTC', fragment3: 'CGTC'},
            fragment2: {fragment4: 'TCAC'},
            fragment3: {},
            fragment4: {fragment3: 'CGTC'}
        }
        ordered_fragments = [fragment1, fragment2, fragment4, fragment3]
        assembler = ChromosomeAssembler(adjacency_map, ordered_fragments)
        chromosome = assembler.assemble_chromosome()

        assert chromosome == 'ACGTCACGTCAA'

    def test_build_chromosome_circular(self):
        """
        Two fragments can each follow the other in a circular arrangement.
        """
        fragment1 = DNAFragment('GACGACG')
        fragment2 = DNAFragment('CGACGA')
        fragment3 = DNAFragment('GACGTT')

        adjacency_map = {
            fragment1: {fragment2: 'CGACG', fragment3: 'GACG'},
            fragment2: {fragment1: 'GACGA'},
            fragment3: {}
        }
        ordered_fragments = [fragment2, fragment1, fragment3]
        assembler = ChromosomeAssembler(adjacency_map, ordered_fragments)
        chromosome = assembler.assemble_chromosome()

        assert chromosome == 'CGACGACGTT'
